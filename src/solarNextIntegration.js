class SolarNextIntegration {

    #urlToSend = "https://sistema.solarnext.app/api/cadastroleadexterno/add";
    #idCampaign = null;
    #formSelector = "";
    #avaliableFields = [
        {toSend: 'nome', toConfigure: 'nome'},
        {toSend: 'email', toConfigure: 'email'},
        {toSend: 'contato', toConfigure: 'contato'},
        {toSend: 'consumo_energia', toConfigure: 'consumo_energia'},
        {toSend: 'cidade', toConfigure: 'cidade'},
        {toSend: 'estado', toConfigure: 'estado'},
        {toSend: 'consumo_energia_valor', toConfigure: 'consumo_energia_valor'},
        {toSend: 'como_contato', toConfigure: 'como_contato'},
        {toSend: 'nivelinteresse', toConfigure: 'nivel_interesse'},
    ];
    #mapSelectores = [];
    #notifications = {
        success: {
            class: "alert-sn-success",
            message: "Anotado! Agora basta aguardar nosso contato!",
            time: 2000,
            show: true
        },
        error: {
            class: "alert-sn-error",
            message: "Ops! Ocorreu um erro ao salvar!",
            time: 2000,
            show: true
        },
    }
    #nomeEvento = "dataSendToSolarNextDone";
    #dispararEvento = true;
    

    constructor(idCampaign, formSelector) {
        if (!idCampaign) {
            throw new Error("It is necessary to inform the campaign code!");
        }
        if (!formSelector){
            throw new Error("It is necessary to inform the form selector!");
        }
        this.#idCampaign = idCampaign;
        this.#formSelector = formSelector;
        this.#configureEnvironment();
        console.info("Integration with SolarNext Plataform was started");
    }

    /* Configuring default data */

    #prepareJquery() {
        if (typeof window.jQuery !== 'function') {
            throw new Error('jQuery is not loaded');
        }
        this.$ = window.jQuery;
    }

    #prepareSelectors() {
        this.#avaliableFields.forEach(({toConfigure}) => {
            this.#fieldSelector = {toConfigure, selector: null};
        });
    }

    #setLoadingDiv() {
        this.$('body').append(`<div class="loading hidden">
          <div class="uil-ring-css" style="transform:scale(0.79);">
            <div></div>
          </div>
        </div>`);
    }

    #removeEventsListeners() {
        this.$(this.#formSelector).on('submit', (e) => e.preventDefault());
    }

    #loadCSS() {
        var cssLink = this.$("<link>");
        this.$("head").append(cssLink); //IE hack: append before setting href
        cssLink.attr({
            rel: "stylesheet",
            type: "text/css",
            href: "//static.solarnext.app/libs/integracao-crm/solarNextIntegration.css"
        });
    }

    #configureOnWindow() {
        const id = this.#generateId;
        const nome = `SolarNextIntegration_${id}`;
        window[nome] = this;
        this.$(this.#formSelector).attr('data-solarnextintegration', nome);
    }

    #configureEnvironment() {
        this.#prepareJquery();
        this.#removeEventsListeners();
        this.#loadCSS();
        this.#prepareSelectors();
        this.#setLoadingDiv();
        this.#configureOnWindow();
    }

    /* Public Methods */

    clearEvents(selector) {
        this.$(selector).unbind();
    }

    avaliableFields() {
        const mapedFields = this.#avaliableFields.map(({toConfigure}) =>  toConfigure);
        console.info(`Avaliable fields: ${mapedFields}`);
    }

    configureField(name, selector) {
        const field = this.#finderAvaliableField(name);
        if(field) {
            this.#fieldSelector = {field: name, selector};
        } else {
            console.warn(`The field name "${name}" is not a valid field name.`);
            this.avaliableFields();
        }
    }

    listen() {
        if (this.#mapSelectores.length) {
            this.$(this.#formSelector).on('submit', this.send);
        } else {
            throw new Error('No fields are configured.');
        }
    }

    send(e) {
        e.preventDefault();
        const name = window.jQuery(this).data('solarnextintegration');
        window[name].runSend(this);
    }

    runSend(element) {
        const toSend = this.#generateToSend(element);
        if (!toSend.nome) {
            throw new Error(`Field Name can't be null`);
        }
        if (!toSend.email && !toSend.contato) {
            throw new Error(`Email or Phone number must be provided.`);
        }
        this.$.ajax({
            type: "POST",
            url: this.#urlToSend,
            data: toSend,
            beforeSend: () =>  this.#showLoading(),
            success: () => {
                this.#notify('success');
                if (this.#dispararEvento) {
                    const event = new Event(this.#nomeEvento);
                    window.document.dispatchEvent(event);
                }
            },
            error: (jqXHR, textStatus, errorThrown) => {
                console.log(jqXHR, textStatus, errorThrown);
                this.#notify('error');
            },
            complete: () => {
                this.#hideLoading();
            }
        });
    }

    /* Helpers */

    #_hideLoadingState(status){
        this.$('.loading').toggleClass("hidden", status);
    }

    #showLoading() {
        this.#_hideLoadingState(false);
    }

    #hideLoading() {
        this.#_hideLoadingState(true);
    }

    #finderAvaliableField(searched) {
        return this.#avaliableFields.find(
            ({ toConfigure }) => toConfigure === searched
        );
    }

    #generateToSend(element) {
        let retorno = {
            id_campanha: this.#idCampaign
        };
        for (let {field, selector} of this.#mapSelectores) {
            if (selector) {
                const elementSearch = this.$(element).find(`[name="${selector}"]`);
                const value = this.$(elementSearch).val();
                if (value) {
                    retorno[field] = value;
                }
            }
        }
        return retorno;
    }

    #notify(type) {
        const config = this.#notifications[type];
        if (config.show) {
            this.$(`
                <div class="alert-sn ${config.class}" id="alert_sn" style="display:none">
                    ${config.message}
                </div>
            `).appendTo("body").show("fast");
            setTimeout(() => {
                window.jQuery("#alert_sn").hide("fold");
                setTimeout(() => {
                    window.jQuery("#alert_sn").remove();
                }, 3000);
            }, 2000 + config.time);
        }
    }

    /* Gets e Sets */

    get #generateId() {
        var max = 0;
        var min = 8;
        var length = Math.floor(Math.random() * (max - min)) + min;
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }


    set #fieldSelector(element) {
        this.#mapSelectores.push(element)
    }

    // Configure Success Notification
    set successMessage(mensagem) {
        this.#notifications.success.message = mensagem;
    }

    set successMessageShow(status) {
        this.#notifications.success.show = status;
    }

    set successMessageClass(classes) {
        this.#notifications.success.class = classes;
    }

    set successMessageTime(time) {
        this.#notifications.success.time = time;
    }

    // Configure Error Notification
    set errorMessage(mensagem) {
        this.#notifications.error.message = mensagem;
    }

    set errorMessageShow(status) {
        this.#notifications.error.show = status;
    }

    set errorMessageClass(classes) {
        this.#notifications.error.class = classes;
    }

    set errorMessageTime(time) {
        this.#notifications.error.time = time;
    }
}