# SolarNext - Integração CRM 

Esta biblioteca foi desenvolvida com o intúito de facilitar a integração das páginas de captura de leads de nossos clientes com o CRM da plataforma.

## Sumário
- [Requisitos](#requisitos)
- [Exemplos](#exemplos)
- [Configuração Básica](#configuração-básica)
    - [1º Passo - Carregando a biblioteca](#basico-etapa-1)
    - [2º Passo - Iniciando a configuração](#basico-etapa-2)
    - [3º Passo - Mapear os atributos](#basico-etapa-3)
    - [4º Passo - Finalizando Configuração](#basico-etapa-4)
    - [O que fazer agora?](#basico-e-agora)
- [Campos, Métodos, Personalização & Erros](#detalhes-integracao)
    - [Campos](#campos)
    - [Métodos](#métodos)
    - [Personalização](#personalização)
    - [Eventos](#eventos)
    - [Erros](#erros)

## Requisitos
Para que seja possível utilizar a biblioteca em seu site, será necessário se certificar que os seguintes requisitos estão sendo cumpridos:

- Ser um assinante ativo da **Plataforma SolarNext**
- Para a página de captura
  - Acesso para adição de um script JavaScript
  - jQuery
***
## Exemplos
Nesse repositório, no diretório *examples* você encontrará diversas formas de configuração mostrando seu funcionamento. Fique livre para olhar.
***

## Configuração Básica 

Veja agora, um rápido passo-a-passo de como realizar a sua primeira configuração. Para isso, vamos utilizar o seguinte formulário como exemplo:
```html
  <form name="formulario_captura" method="post">
    <!-- Nome -->
    <div>
      <label for="nome">Nome:</label>
      <input type="text" id="nome" name="nome" required />
    </div>
    <!-- Email -->
    <div>
      <label for="email">Email:</label>
      <input type="email" id="email" name="email" required />
    </div>
    <!-- Telefone -->
    <div>
      <label for="telefone">Telefone:</label>
      <input type="text" id="telefone" name="telefone" />
    </div>
    <!-- Cidade -->
    <div>
      <label for="cidade">Cidade:</label>
      <input type="text" id="cidade" name="cidade" />
    </div>
    <!-- Valor Pago -->
    <div>
      <label for="valor_consumo">Valor Consumo de Energia:</label>
      <input type="text" id="valor_consumo" name="valor_consumo" />
    </div>
    <!-- Botão para envio -->
    <button type="submit">Cadastrar</button>
  </form>
 ```
<a id="basico-etapa-1"></a>

### 1º Passo - Carregando a biblioteca {#basico-etapa-1}

Para Realizar o carregamento da biblioteca, adicione o seguinte código após a tag `</body>`:
```html
<script src="//static.solarnext.app/libs/integracao-crm/solarNextIntegration.js"></script>
```
<a id="basico-etapa-2"></a>

### 2º Passo - Iniciando a configuração

Vamos começar agora a configurar a biblioteca. Logo abaixo do código inserido na etapa anterior, adicione o seguinte trecho:
```html
<script>
    let integracaoSN = null;
    window.jQuery(document).ready(() => {
        integracaoSN = new SolarNextIntegration(COD_CAMPANHA, FORM_SELECTOR);
        // Próximos passos
    });
</script>
```
onde:
- `COD_CAMPANHA` é o código da campanha no CRM da SolarNext;
- `FORM_SELECTOR` é a forma na qual o jQuery seleciona o formulário desejado. Para ajudar nesse exmplo, vamos usar `"form[name='formulario_captura']"` para selecionar o formulário que queremos;

ficando assim: 

```html
<script>
    let integracaoSN = null;
    window.jQuery(document).ready(() => {
        integracaoSN = new SolarNextIntegration(ID_CAMPANHA, "form[name='formulario_captura']");
        // Próximos passos
    });
</script>
```
<a id="basico-etapa-3"></a>

### 3º Passo - Mapear os atributos

Agora que já realizamos a configuração básica, está na hora de realizarmos o mapeamento dos dados do formulário para a forma que a biblioteca trabalha. Obs.: Todos os campos disponíveis são listados na seção [Campos](#campos).

```html
<script>
    let integracaoSN = null;
    window.jQuery(document).ready(() => {
        integracaoSN = new SolarNextIntegration(COD_CAMPANHA, "form[name='formulario_captura']");
        // O campo "nome" recebará o valor do input com o atributo name="nome"
        integracaoSN.configureField("nome", "nome"); 
        // O campo "email" recebará o valor do input com o atributo name="email"
        integracaoSN.configureField("email", "email"); 
        // O campo "contato" recebará o valor do input com o atributo name="telefone"
        integracaoSN.configureField("contato", "telefone"); 
        // O campo "cidade" recebará o valor do input com o atributo name="cidade"
        integracaoSN.configureField("cidade", "cidade"); 
        // O campo "consumo_energia_valor" recebará o valor do input com o atributo name="valor_consumo"
        integracaoSN.configureField("consumo_energia_valor", "valor_consumo"); 
        // Próximos passos
    });
</script>
```
<a id="basico-etapa-4"></a>

### 4º Passo - Finalizando Configuração
O último passo é informar para a biblioteca que já terminamos as configurações e que, agora, ela pode começar a esperar que o formulário seja submetido. Para isso, basta executar o comando `listen()`. O código final ficará assim:

```html
<script>
    let integracaoSN = null;
    window.jQuery(document).ready(() => {
        integracaoSN = new SolarNextIntegration(COD_CAMPANHA, "form[name='formulario_captura']");
        // O campo "nome" recebará o valor do input com o atributo name="nome"
        integracaoSN.configureField("nome", "nome"); 
        // O campo "email" recebará o valor do input com o atributo name="email"
        integracaoSN.configureField("email", "email"); 
        // O campo "contato" recebará o valor do input com o atributo name="telefone"
        integracaoSN.configureField("contato", "telefone"); 
        // O campo "cidade" recebará o valor do input com o atributo name="cidade"
        integracaoSN.configureField("cidade", "cidade"); 
        // O campo "consumo_energia_valor" recebará o valor do input com o atributo name="valor_consumo"
        integracaoSN.configureField("consumo_energia_valor", "valor_consumo"); 
        // Esperando o formulário ser submetido
        integracaoSN.listen();
    });
</script>
```
Pronto, agora toda vez que o formulário for submetido, os dados serão enviados para o CRM da SolarNext.


<a id="basico-e-agora"></a>

### O que fazer agora?

Você pode visualizar o restante da documentação e descobrir todos os métodos disponíveis na biblioteca.
***

<a id="detalhes-integracao"></a>

## Campos, Métodos, Personalização & Erros
Nesta seção serão listados e detalhados todos os [campos](#campos) de formulário aceitos, [métodos](#metodos) disponíveis na biblioteca, como [personalizar](#personalização) as notificações, [eventos](#eventos) disparados e os possíveis [erros](#erros) que podem aparecer.

### Campos
A seguir a tabela apresenta descrição e os campos que estão disponíveis para serem cadastrados no CRM da SolarNext.

|          Atributo         |          Descrição        |
|---------------------------|---------------------------|
|`nome`                     |  Nome do cliente          |
|`email`                    |  Email para contato       |
|`contato`                  |  Telefone de contato      |
|`cidade`                   |  Cidade do cliente        |
|`consumo_energia`          |  KWh de consumo           |
|`consumo_energia_valor`    |  Valor pago pelo consumo  |

***CAMPOS OBRIGATÓRIOS:***

- `nome` 
- `contato` ou `email`.

### Métodos
Nessa seção, serão listados e descritos os métodos disponíveis para configuração e uso da biblioteca. São eles:

- `clearEvents(selector)` 
    - Descrição:
        - Nos formulários selecionados, remove os outros ouvintes do evento "submit". Isso permite que você desabilite o efeito de possíveis outras bibliotecas que estejam presentes na plataforma. 
    - Parâmetros:
        - *selector* - Seletor jQuery para os elementos que você deseja que seja aplicado.
    - Restrição de aplicação:
        - Deve ser aplicado somente antes do comando `listen()` ser executado.

- `avaliableFields()`
    - Descrição: 
        - Lista no console, todos os campos disponíveis.

- `configureField(name, selector)`
    - Descrição:
        - Mapeia o campo e o name do input no formulário.
    - Parâmetros:
        - *name* - Nome do campo que será configurado.
        - *selector* - Deve ser o conteúdo que está no atributo name do input no formulário.
    - Retrição de aplicação:
        - Deve ser aplicado somente antes do comando `listen()` ser executado.


- `listen()`
    - Descrição:
        - Configura para que quando o formulário for submetido, o método `send(e)` seja executado.
    - Restrição de Aplicação:
        - Deve ser executado somente quando a configuração da biblioteca estiver completa.

- `send(e)`
    - Descrição:
        - Disparado quando o formulário é submetido, ele recupera a configuração de integração e executa o comando `runSend(element)`.
    - Parâmetros:
        - *e* - Evento de origem.
    - Restrição de aplicação:
        - NÃO deve ser chamado manualmente.

- `runSend(element)`
    - Descrição:
        - Prepara todos os dados e envia para o CRM da SolarNext.
    - Parâmetros:
        - *element* - Elemento DOM do formulário que foi submetido.
    - Restrição de aplicação:
        - Sua chamada manual deve ser feita somente com a configuração básica e o mapeamento estiverem feitos completamente.

### Personalização

Os seguintes itens podem ser personalizados na biblioteca:
- Notificação de sucesso:
    - Mensagem:
        - Tipo: String
        - Descrição: Mensagem que será exibida na barra de notificação.
        - Padrão: `Anotado! Agora basta aguardar nosso contato!`
        - Atributo: `successMessage`
    - Exibir:
        - Tipo: Boolean
        - Descrição: Indica se a notificação deve ser exibida.
        - Padrão: `true`
        - Atributo: `successMessageShow`
    - Class:
        - Tipo: String
        - Descrição: Conteúdo do atributo class da notificação.
        - Padrão: `alert-sn-success`
        - Atributo: `successMessageClass`
    - Time:
        - Tipo: Number
        - Descrição: Indica o tempo em *ms* em que a notificação ficará sendo exibida
        - Padrão: `2000`
        - Atributo: `successMessageTime`

- Notificação de erro:
    - Mensagem:
        - Tipo: String
        - Descrição: Mensagem que será exibida na barra de notificação.
        - Padrão: `Ops! Ocorreu um erro ao salvar!`
        - Atributo: `errorMessage`
    - Exibir:
        - Tipo: Boolean
        - Descrição: Indica se a notificação deve ser exibida.
        - Padrão: `true`
        - Atributo: `errorMessageShow`
    - Class:
        - Tipo: String
        - Descrição: Conteúdo do atributo class da notificação.
        - Padrão: `alert-sn-error`
        - Atributo: `errorMessageClass`
    - Time:
        - Tipo: Number
        - Descrição: Indica o tempo em *ms* em que a notificação ficará sendo exibida
        - Padrão: `2000`
        - Atributo: `errorMessageTime`

Para personalizar, basta executar o seguinte comando:
```
integracaoSN.ATRIBUTO = VALOR 
```
onde, `ATRIBUTO` é o atributo do elemento que você deseja alterar e `VALOR` é o valor que ele receberá.

### Eventos

Quando um cadastro é feito com sucesso, por padrão um evento no objeto `window.document` é emitido como o nome *dataSendToSolarNextDone*. Isso é últio caso você deseje realizar alguma ação após o cadastro ser realizado.

### Erros

Alguns erros durante a configuração podem ocorrer. Eles serão mostrados no console de desenvolvedor do navegador. Os erros são os seguintes:
- `It is necessary to inform the campaign code!` - Esse erro ocorre na configuração inicial da integração quando o ID da Campanha do CRM da SolarNext não é informado. 
- `It is necessary to inform the form selector!` - Da mesma origem do erro anterior, no entanto, informa que a seletor do formulário não foi fornecido na configuração.
- `jQuery is not loaded` - Esse erro ocorre quando o jQuery não foi carregado na página.
- `No fields are configured.` - Acontece quando o comando `listen()` é executado porém não existem campos configurados.
- `Field Name can't be null` - Ocorre quando há a tentativa de cadastro mas o campo relacionado ao nome não foi preenchido.
- `Email or Phone number must be provided.` - Ocorre quando na tentantiva de cadastro o email ou telefone não foi informado.